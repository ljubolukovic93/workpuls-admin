export function checkStringAndConvertDate(date: string | Date) {
    if (!(date instanceof Date)) {
        date = new Date(date)
    }
    return date
}

export function isSameDay(d1: string | Date, d2: string | Date) {
    d1 = checkStringAndConvertDate(d1)
    d2 = checkStringAndConvertDate(d2)
    return d1.getFullYear() === d2.getFullYear() &&
        d1.getMonth() === d2.getMonth() &&
        d1.getDate() === d2.getDate()
}

export function hoursDifference(d1: string | Date, d2: string | Date) {
    d1 = checkStringAndConvertDate(d1)
    d2 = checkStringAndConvertDate(d2)
    const milliseconds = Math.abs(d2.getTime() - d1.getTime())
    return milliseconds / 36e5
}

export function isSecondGreater(d1: string | Date, d2: string | Date) {
    d1 = checkStringAndConvertDate(d1)
    d2 = checkStringAndConvertDate(d2)
    return d1.getTime() < d2.getTime()
}

export function getDateOnly(date: string | Date) {
    date = checkStringAndConvertDate(date)
    return date.toDateString()
}
