import { Injectable } from '@angular/core'
import { DashboardData, Employee } from '../model'
import { BehaviorSubject, Subject } from 'rxjs'
import { getDateOnly, hoursDifference, isSameDay } from '../util/date-util'
import { HttpClient } from '@angular/common/http'
import { tap } from 'rxjs/operators'

@Injectable({
    providedIn: 'root'
})
export class EmployeeService {

    employeesSubject: Subject<Employee[]> = new BehaviorSubject<Employee[]>([])
    employeesOverviewSubject: Subject<DashboardData> = new BehaviorSubject<DashboardData>(null)

    constructor(private _http: HttpClient) { }

    getAll() {
        return this._http.get('/employees').pipe(
            tap(data => {
                this.populateEmployees(data)
            })
        )
    }

    updateAll(employees: Employee[]) {
        return this._http.put<Employee[]>('/employees', employees).pipe(
            tap(data => {
                this.populateEmployees(data)
            })
        )
    }

    populateEmployees(data?: any) {
        this.employeesSubject.next(data)
        this.calculateSummary(data)
    }

    calculateSummary(data: Employee[]) {
        const dashboardData: DashboardData = { totalEmployees: 0, totalClockedInTime: 0, paidForOvertimeHours: 0, paidForRegularHours: 0 }
        data.forEach(
            employee => {
                const employeeDates = {}
                if (employee.active) {
                    employee.shifts.forEach(
                        shift => {
                            if (isSameDay(shift.clockIn, shift.clockOut)) {
                                const dateOnly = getDateOnly(shift.clockIn)
                                if (!employeeDates[dateOnly]) {
                                    employeeDates[dateOnly] = 0
                                }
                                employeeDates[dateOnly] += hoursDifference(shift.clockIn, shift.clockOut)
                            } else {
                                let date3 = new Date(Date.parse(shift.clockOut))
                                date3 = new Date(date3.setHours(0, 0, 0, 0))
                                const date1Only = getDateOnly(shift.clockIn)
                                if (!employeeDates[date1Only]) {
                                    employeeDates[date1Only] = 0
                                }
                                employeeDates[date1Only] += hoursDifference(shift.clockIn, date3)

                                const date2Only = getDateOnly(shift.clockOut)
                                if (!employeeDates[date2Only]) {
                                    employeeDates[date2Only] = 0
                                }
                                employeeDates[date2Only] += hoursDifference(date3, shift.clockOut)
                            }
                        }
                    )
                    let regularHours = 0
                    let overtimeHours = 0
                    Object.keys(employeeDates).forEach(
                        date => {
                            if (employeeDates[date] > 8) {
                                overtimeHours += employeeDates[date] - 8
                                regularHours += 8
                            } else {
                                regularHours += employeeDates[date]
                            }
                        }
                    )
                    employee.paidForRegularHours = employee.hourlyRate * regularHours
                    employee.paidForOvertimeHours = employee.overtimeHourlyRate * overtimeHours
                    employee.totalHours = regularHours + overtimeHours

                    dashboardData.totalEmployees++
                    dashboardData.totalClockedInTime += regularHours + overtimeHours
                    dashboardData.paidForRegularHours += employee.paidForRegularHours
                    dashboardData.paidForOvertimeHours += employee.paidForOvertimeHours
                }
            }
        )
        this.employeesOverviewSubject.next(dashboardData)
    }

}
