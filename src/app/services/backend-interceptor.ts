import { Injectable } from '@angular/core'
import { HttpInterceptor, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { delay } from 'rxjs/operators'
import { Employee } from '../model'

let EMPLOYEES_DATA: Employee[] = [
    {
        id: 1, name: 'Petar Petrovic', email: 'pera@gmail.com', hourlyRate: 10, overtimeHourlyRate: 15, active: true,
        shifts: [
            { clockIn: '2021-02-02T08:00:00', clockOut: '2021-02-02T16:00:00' },
            { clockIn: '2021-02-03T08:00:00', clockOut: '2021-02-03T16:00:00' },
            { clockIn: '2021-02-09T08:00:00', clockOut: '2021-02-09T16:00:00' },
            { clockIn: '2021-02-10T08:00:00', clockOut: '2021-02-10T17:00:00' },
            { clockIn: '2021-02-11T08:00:00', clockOut: '2021-02-11T16:00:00' },
            { clockIn: '2021-02-12T08:00:00', clockOut: '2021-02-12T17:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T01:00:00' },
            { clockIn: '2021-02-17T08:00:00', clockOut: '2021-02-17T17:00:00' },
        ]
    },
    {
        id: 2, name: 'Jovan Jovanic', email: 'jovan@gmail.com', hourlyRate: 10, overtimeHourlyRate: 20, active: true,
        shifts: [
            { clockIn: '2021-02-02T08:00:00', clockOut: '2021-02-02T16:00:00' },
            { clockIn: '2021-02-03T08:00:00', clockOut: '2021-02-03T16:00:00' },
            { clockIn: '2021-02-11T08:00:00', clockOut: '2021-02-11T16:00:00' },
            { clockIn: '2021-02-12T08:00:00', clockOut: '2021-02-12T17:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T08:00:00', clockOut: '2021-02-15T16:00:00' },
        ]
    },
    {
        id: 3, name: 'Jovana Dimitrijevic', email: 'jocaa@gmail.com', hourlyRate: 10, overtimeHourlyRate: 15, active: true,
        shifts: [
            { clockIn: '2021-02-02T08:00:00', clockOut: '2021-02-02T16:00:00' },
            { clockIn: '2021-02-03T08:00:00', clockOut: '2021-02-03T16:00:00' },
            { clockIn: '2021-02-11T08:00:00', clockOut: '2021-02-11T16:00:00' },
            { clockIn: '2021-02-12T08:00:00', clockOut: '2021-02-12T17:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T01:00:00' },
            { clockIn: '2021-02-17T08:00:00', clockOut: '2021-02-17T17:00:00' }
        ]
    },
    {
        id: 4, name: 'Milos Tomic', email: 'milost@gmail.com', hourlyRate: 9, overtimeHourlyRate: 14, active: true,
        shifts: [
            { clockIn: '2021-02-11T08:00:00', clockOut: '2021-02-11T16:00:00' },
            { clockIn: '2021-02-12T08:00:00', clockOut: '2021-02-12T17:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T01:00:00' },
            { clockIn: '2021-02-17T08:00:00', clockOut: '2021-02-17T17:00:00' }
        ]
    },
    {
        id: 5, name: 'Dejan Matic', email: 'deki@icloud.com', hourlyRate: 10, overtimeHourlyRate: 15, active: true,
        shifts: [
            { clockIn: '2021-02-02T08:00:00', clockOut: '2021-02-02T16:00:00' },
            { clockIn: '2021-02-03T08:00:00', clockOut: '2021-02-03T16:00:00' },
            { clockIn: '2021-02-11T08:00:00', clockOut: '2021-02-11T16:00:00' },
            { clockIn: '2021-02-12T08:00:00', clockOut: '2021-02-12T17:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T01:00:00' },
            { clockIn: '2021-02-17T08:00:00', clockOut: '2021-02-17T17:00:00' }
        ]
    },
    {
        id: 6, name: 'Milica Mitic', email: 'milica90@yahoo.com', hourlyRate: 12, overtimeHourlyRate: 16, active: true,
        shifts: [
            { clockIn: '2021-02-02T08:00:00', clockOut: '2021-02-02T16:00:00' },
            { clockIn: '2021-02-03T08:00:00', clockOut: '2021-02-03T16:00:00' },
            { clockIn: '2021-02-15T08:00:00', clockOut: '2021-02-15T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T01:00:00' },
            { clockIn: '2021-02-17T08:00:00', clockOut: '2021-02-17T17:00:00' }
        ]
    },
    {
        id: 7, name: 'Nenad Maksimovic', email: 'maksa@gmail.com', hourlyRate: 12, overtimeHourlyRate: 16, active: true,
        shifts: [
            { clockIn: '2021-02-11T16:00:00', clockOut: '2021-02-12T00:00:00' },
            { clockIn: '2021-02-12T16:00:00', clockOut: '2021-02-13T00:00:00' },
            { clockIn: '2021-02-15T16:00:00', clockOut: '2021-02-16T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T00:00:00' },
            { clockIn: '2021-02-17T16:00:00', clockOut: '2021-02-18T00:00:00' },
        ]
    },
    {
        id: 8, name: 'Uros Ivkovic', email: 'uros.ivko@gmail.com', hourlyRate: 15, overtimeHourlyRate: 18, active: true,
        shifts: [
            { clockIn: '2021-02-11T16:00:00', clockOut: '2021-02-12T00:00:00' },
            { clockIn: '2021-02-12T16:00:00', clockOut: '2021-02-13T00:00:00' },
            { clockIn: '2021-02-15T16:00:00', clockOut: '2021-02-16T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T00:00:00' },
            { clockIn: '2021-02-17T16:00:00', clockOut: '2021-02-18T00:00:00' }
        ]
    },
    {
        id: 9, name: 'Milos Smiljkovic', email: 'smiljko@telekom.rs', hourlyRate: 15, overtimeHourlyRate: 18, active: true,
        shifts: [
            { clockIn: '2021-02-11T16:00:00', clockOut: '2021-02-12T00:00:00' },
            { clockIn: '2021-02-12T16:00:00', clockOut: '2021-02-13T00:00:00' },
            { clockIn: '2021-02-15T16:00:00', clockOut: '2021-02-16T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T00:00:00' },
            { clockIn: '2021-02-17T16:00:00', clockOut: '2021-02-18T00:00:00' }
        ]
    },
    {
        id: 10, name: 'Filip Smiljevic', email: 'uros.ivko@gmail.com', hourlyRate: 15, overtimeHourlyRate: 18, active: true,
        shifts: [
            { clockIn: '2021-02-11T16:00:00', clockOut: '2021-02-12T00:00:00' },
            { clockIn: '2021-02-12T16:00:00', clockOut: '2021-02-13T00:00:00' },
            { clockIn: '2021-02-15T16:00:00', clockOut: '2021-02-16T16:00:00' },
            { clockIn: '2021-02-16T15:00:00', clockOut: '2021-02-17T00:00:00' },
            { clockIn: '2021-02-17T16:00:00', clockOut: '2021-02-18T00:00:00' }
        ]
    },
]

@Injectable()
export class BackendInterceptor implements HttpInterceptor {

    intercept(request, next: HttpHandler): Observable<HttpEvent<any>> {
        if (request.method === 'GET' && request.url === '/employees') {
            return of(new HttpResponse({ status: 200, body: EMPLOYEES_DATA })).pipe(delay(300))
        }
        if (request.method === 'PUT' && request.url === '/employees') {
            let newData = []
            EMPLOYEES_DATA.forEach(
                employee => {
                    const found = request.body.find(emp => emp.id === employee.id)
                    if (found) {
                        newData = [...newData, { ...employee, ...found }]
                    } else {
                        newData = [...newData, employee]
                    }
                }
            )
            EMPLOYEES_DATA = newData
            return of(new HttpResponse({ status: 200, body: EMPLOYEES_DATA })).pipe(delay(300))
        }
        next.handle(request)
    }
}
