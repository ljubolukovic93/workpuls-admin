import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { AppComponent } from './app.component'
import { DashboardComponent } from './components/dashboard/dashboard.component'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { EmployeesComponent } from './components/employees/employees.component'
import { DashboardDataComponent } from './components/dashboard-data/dashboard-data.component'
import { EmployeesEditDialogComponent } from './components/employees-edit-dialog/employees-edit-dialog.component'
import { AppRoutingModule } from './app-routing.module'
import { MaterialModule } from './material.module'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http'
import { BackendInterceptor } from './services/backend-interceptor'

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    EmployeesComponent,
    DashboardDataComponent,
    EmployeesEditDialogComponent
  ],
  entryComponents: [EmployeesEditDialogComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: BackendInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
