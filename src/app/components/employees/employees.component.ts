import { Component, Input } from '@angular/core'
import { SelectionModel } from '@angular/cdk/collections'
import { MatTableDataSource } from '@angular/material/table'
import { Employee } from 'src/app/model'
import { MatDialog } from '@angular/material/dialog'
import { EmployeesEditDialogComponent } from '../employees-edit-dialog/employees-edit-dialog.component'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent {

  displayedColumns: string[] = ['select', 'name', 'email', 'totalTime', 'totalPaidRegular', 'totalPaidOvertime']

  @Input() set employees(employees: Employee[]) {
    if (employees) {
      this.dataSource = new MatTableDataSource<Employee>(employees)
      this.selection.clear()
    }
  }

  dataSource: MatTableDataSource<Employee>
  selection = new SelectionModel<Employee>(true, [])

  constructor(public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length
    const numRows = this.dataSource.data.length
    return numSelected === numRows
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row))
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: Employee): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`
  }

  openBulkEditDialog() {
    if (this.selection.isEmpty()) {
      this._snackBar.open('Select employees first!', null, {
        duration: 3000,
      })
      return
    }
    const dialogRef = this.dialog.open(EmployeesEditDialogComponent, {
      data: this.selection.selected,
      width: '640px',
    })
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this._snackBar.open('Data updated!', null, {
            duration: 4000,
          })
        }
      }
    )
  }

}
