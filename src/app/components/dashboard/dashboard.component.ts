import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core'
import { BehaviorSubject } from 'rxjs'
import { EmployeeService } from 'src/app/services/employee.service'

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DashboardComponent implements OnInit {

  loading$ = new BehaviorSubject<boolean>(true)

  constructor(public employee: EmployeeService) { }

  ngOnInit(): void {
    this.employee.getAll().subscribe(
      data => this.loading$.next(false),
      err => this.loading$.next(false)
    )
  }

}
