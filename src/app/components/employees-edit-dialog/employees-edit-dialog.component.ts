import { Component, Inject, OnInit } from '@angular/core'
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms'
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog'
import { Employee } from 'src/app/model'
import { EmployeeService } from 'src/app/services/employee.service'
import { datesRangeValidator } from './dates.validator'

@Component({
  selector: 'app-employees-edit-dialog',
  templateUrl: './employees-edit-dialog.component.html',
  styleUrls: ['./employees-edit-dialog.component.scss']
})
export class EmployeesEditDialogComponent implements OnInit {

  bulkForm: FormArray
  activeStatus = [
    { label: 'Active', value: true },
    { label: 'Inactive', value: false },
  ]

  constructor(
    private _formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<EmployeesEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Employee[],
    private _employee: EmployeeService) { }

  ngOnInit(): void {
    if (this.data && this.data[0]) {
      const array = []
      this.data.forEach(
        employee => {
          const employeeForm = this._formBuilder.group({
            id: '',
            name: new FormControl(null, [Validators.required]),
            active: new FormControl(false, [Validators.required]),
            // email: new FormControl(null, [Validators.required, Validators.email]),
            hourlyRate: new FormControl(null, [Validators.required, Validators.min(0)]),
            overtimeHourlyRate: new FormControl(null, [Validators.required, Validators.min(0)]),
            shifts: new FormArray([])
          })
          employee.shifts.forEach(
            shift => {
              const shiftArrayForm = employeeForm.get('shifts') as FormArray
              shiftArrayForm.push(new FormGroup({
                clockIn: new FormControl(shift.clockIn, [Validators.required]),
                clockOut: new FormControl(shift.clockOut, [Validators.required])
              }, datesRangeValidator))
            }
          )
          employeeForm.patchValue(employee)
          array.push(employeeForm)
        }
      )
      this.bulkForm = new FormArray(array)
    }
  }

  saveBulk() {
    this.bulkForm.updateValueAndValidity()
    if (this.bulkForm.valid) {
      const value = this.bulkForm.getRawValue()
      this._employee.updateAll(value).subscribe(
        data => this.dialogRef.close(true)
      )
    }
  }

}
