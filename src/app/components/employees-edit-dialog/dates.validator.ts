import { FormGroup } from '@angular/forms'
import { isSecondGreater } from 'src/app/util/date-util'

export function datesRangeValidator(frm: FormGroup) {
    if (!frm.controls['clockIn'].value || !frm.controls['clockOut'].value) {
        return
    }
    return isSecondGreater(frm.controls['clockIn'].value, frm.controls['clockOut'].value) ? null : { datesInvalid: true }
}
