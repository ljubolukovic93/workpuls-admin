import { Component, Input, OnInit } from '@angular/core'
import { DashboardData } from 'src/app/model'

@Component({
  selector: 'app-dashboard-data',
  templateUrl: './dashboard-data.component.html',
  styleUrls: ['./dashboard-data.component.scss']
})
export class DashboardDataComponent implements OnInit {

  @Input() dashboardData: DashboardData

  constructor() { }

  ngOnInit(): void {
  }

}
