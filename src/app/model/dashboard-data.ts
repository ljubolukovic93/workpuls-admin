export interface DashboardData {
    totalEmployees: number
    totalClockedInTime: number
    paidForRegularHours: number
    paidForOvertimeHours: number
}
