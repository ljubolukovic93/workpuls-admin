import { Shift } from './shift'

export interface Employee {
    id: number
    name: string
    email: string
    active?: boolean
    hourlyRate: number
    overtimeHourlyRate: number
    totalHours?: number

    paidForRegularHours?: number
    paidForOvertimeHours?: number

    shifts?: Shift[]
}
